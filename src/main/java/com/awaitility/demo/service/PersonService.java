package com.awaitility.demo.service;

import com.awaitility.demo.domain.Person;
import com.awaitility.demo.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

@Service
public class PersonService {

    @Autowired
    PersonRepository personRepository;

    @Async
    public CompletableFuture<Void> updatePersonWith1SecondDelay(Long id, Person person) throws InterruptedException {
        TimeUnit.SECONDS.sleep(1);

        person.setId(id);
        personRepository.save(person);
        return new CompletableFuture<>();
    }

    @Async
    public CompletableFuture<Person> updatePersonWith2SecondsDelay(Long id, Person person) throws InterruptedException {
        TimeUnit.SECONDS.sleep(2);

        person.setId(id);
        personRepository.save(person);
        return new CompletableFuture<>();
    }

    @Async
    public CompletableFuture<Person> updatePersonWith3SecondsDelay(Long id, Person person) throws InterruptedException {
        TimeUnit.SECONDS.sleep(3);

        person.setId(id);
        personRepository.save(person);
        return new CompletableFuture<>();
    }

    @Async
    public CompletableFuture<Person> updatePersonWithDelayFrom1To5Seconds(Long id, Person person) throws InterruptedException {
        int random = new Random().nextInt(5);
        TimeUnit.SECONDS.sleep(random);

        person.setId(id);
        personRepository.save(person);
        return new CompletableFuture<>();
    }
}