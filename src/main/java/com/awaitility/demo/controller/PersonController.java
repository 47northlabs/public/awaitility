package com.awaitility.demo.controller;

import com.awaitility.demo.domain.Person;
import com.awaitility.demo.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class PersonController {

    @Autowired
    PersonService personService;


    @RequestMapping(method = RequestMethod.PUT, path = "/endpoint1/{id}")
    public ResponseEntity<String> updatePersonWith1SecondDelay(@PathVariable Long id, @RequestBody Person person) throws InterruptedException {
        personService.updatePersonWith1SecondDelay(id, person);

        return ResponseEntity.ok().body("Request received");
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/endpoint2/{id}")
    public ResponseEntity<String> updatePersonWith2SecondsDelay(@PathVariable Long id, @RequestBody Person person) throws InterruptedException {
        personService.updatePersonWith2SecondsDelay(id, person);

        return ResponseEntity.ok().body("Request received");
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/endpoint3/{id}")
    public ResponseEntity<String> updatePersonWith3SecondsDelay(@PathVariable Long id, @RequestBody Person person) throws InterruptedException {
        personService.updatePersonWith3SecondsDelay(id, person);

        return ResponseEntity.ok().body("Request received");
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/endpoint4/{id}")
    public ResponseEntity<String> updatePersonWithDelayFrom1To5Seconds(@PathVariable Long id, @RequestBody Person person) throws InterruptedException {
        personService.updatePersonWithDelayFrom1To5Seconds(id, person);

        return ResponseEntity.ok().body("Request received");
    }
}
