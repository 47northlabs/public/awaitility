package com.awaitility.demo.controller;

import com.awaitility.demo.domain.Person;
import com.awaitility.demo.repository.PersonRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.core.StringContains.containsString;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class PersonControllerTest {

    @Autowired
    PersonRepository personRepository;

    @Autowired
    PersonController personController;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testDelay1Second() throws Exception {
        Person person = new Person();
        person.setName("Yara");
        person.setAddress("New York");
        person.setAge("23");
        personRepository.save(person);

        ObjectMapper mapper = new ObjectMapper();

        person.setName("Daenerys");

        this.mockMvc.perform(put("/api/endpoint1/" + person.getId())
                .contentType(APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(person)))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Request received")));

        waitForCompletion();
        assertThat(personRepository.findById(person.getId()).get().getName())
                .isEqualTo("Daenerys");
    }

    @Test
    public void testDelay2Seconds() throws Exception {
        Person person = new Person();
        person.setName("Cersei");
        person.setAddress("Myrtle");
        person.setAge("28");
        personRepository.save(person);

        ObjectMapper mapper = new ObjectMapper();

        person.setName("Sansa");

        this.mockMvc.perform(put("/api/endpoint2/" + person.getId())
                .contentType(APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(person)))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Request received")));

        waitForCompletion();
        assertThat(personRepository.findById(person.getId()).get().getName())
                .isEqualTo("Sansa");
    }

    @Test
    public void testDelay3Seconds() throws Exception{
        Person person = new Person();
        person.setName("Tyrion");
        person.setAddress("Boston");
        person.setAge("28");
        personRepository.save(person);

        ObjectMapper mapper = new ObjectMapper();

        person.setName("John");

        this.mockMvc.perform(put("/api/endpoint3/" + person.getId())
                .contentType(APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(person)))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Request received")));

        waitForCompletion();
        assertThat(personRepository.findById(person.getId()).get().getName())
                .isEqualTo("John");
    }

    @Test
    public void testDelayFrom1To5Seconds() throws Exception{
        Person person = new Person();
        person.setName("Sandor");
        person.setAddress("LA");
        person.setAge("28");
        personRepository.save(person);

        ObjectMapper mapper = new ObjectMapper();

        person.setName("Gregor");

        this.mockMvc.perform(put("/api/endpoint4/" + person.getId())
                .contentType(APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(person)))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Request received")));

        waitForCompletion();
        assertThat(personRepository.findById(person.getId()).get().getName())
                .isEqualTo("Gregor");
    }

    private void waitForCompletion() throws InterruptedException {
        TimeUnit.SECONDS.sleep(5);
    }

}